class Ship:
    positions = []
    hits = []
    type: None


    def __init__(self, description):
        self.positions = description.split(" ")
        self.hits = []
        self.generateCoordinates()

    def generateCoordinates(self):

        # This method should generate the rest of coordinates
        # of the ship, so then we can check if shif was hitten
        # or sunken

        positions = []

        if len(self.positions) == 2:
            initialPosition1 = coordinateToPositions(self.positions[0])
            initialPosition2 = coordinateToPositions(self.positions[1])

            positions.append(initialPosition1)
            positions.append(initialPosition2)

            shipType = getShipType(initialPosition1, initialPosition2)
            self.shipType = shipType

            if shipType == "square":

                sortedInitialPositions = sorted(positions, key=lambda key: key["column"])

                # Redundant
                if sortedInitialPositions[1]["column"] > sortedInitialPositions[0]["column"]:

                   if sortedInitialPositions[1]["row"] > sortedInitialPositions[0]["row"]:


                     coordinate1 = positionToCoordinateString([sortedInitialPositions[1]["row"] - 1, sortedInitialPositions[1]["column"]])
                     coordinate2 = positionToCoordinateString([sortedInitialPositions[0]["row"] + 1, sortedInitialPositions[0]["column"]])

                   else:

                     coordinate1 = positionToCoordinateString([sortedInitialPositions[1]["column"] - 1, sortedInitialPositions[1]["row"]])
                     coordinate2 = positionToCoordinateString([sortedInitialPositions[0]["column"] + 1, sortedInitialPositions[0]["row"]])

                   self.positions.append(coordinate1)
                   self.positions.append(coordinate2)



            elif shipType == "horizontal_rectangle":
                sortedInitialPositions = sorted(positions, key=lambda key: key["column"])

                difference = sortedInitialPositions[1]["column"] - sortedInitialPositions[0]["column"] - 1

                if difference > 2:
                    raise Exception("Ship size is greater than 4 positions")
                else:
                    for i in range(difference):
                        position = [sortedInitialPositions[0]["row"], sortedInitialPositions[0]["column"] + i + 1]

                        self.positions.append(positionToCoordinateString(position))


            elif shipType == "vertical_rectangle":

                    sortedInitialPositions = sorted(positions, key=lambda key: key["row"])

                    difference = sortedInitialPositions[1]["row"] - sortedInitialPositions[0]["row"] - 1


                    if difference > 2:
                        raise Exception("Ship size is greater than 4 positions")
                    else:
                        for i in range(difference):
                            position = [sortedInitialPositions[0]["row"] + i + 1, sortedInitialPositions[0]["column"]]
                            self.positions.append(positionToCoordinateString(position))


        else:
            raise Exception("Only 2 coordinates must be passed in order to generate ship.")
        return []

    def hit(self, position):
        if position in self.positions:
            print("La posición ", position, " está en el ship ", self.positions)
            if position not in self.hits:
                self.hits.append(position)



    def toString(self):

        print("--------------------------")
        print("Ship positions: ", sorted(self.positions))
        print("Ship hits: ", self.hits)
        print("--------------------------")

    def hasBeenHitted(self):
        if len(self.hits) > 0:
            if len(self.hits) == len(self.positions):
                return False
            else:
                return True
        else:
            return False

    def hasBeenSunk(self):
        if len(self.hits) > 0:
            if len(self.hits) == len(self.positions):
                return True
            else:
                return False
        else:
            return False

def getShipType(position1, position2):

    if position1["row"] == position2["row"]:
        return "horizontal_rectangle"
    elif position1["column"] == position2["column"]:
        return "vertical_rectangle"
    else:
        return "square"

def positionToCoordinateString(position):

    row = position[0]

    column = numberToLetter(position[1])

    return column + str(row)

def coordinateToPositions(coordinateString):
    column = parseLetterToNumber(coordinateString[0])
    row = int(coordinateString[1])

    # Returns column and row, in order to calculate
    # missing positions

    return {"row": row, "column": column}

def calculateHitteds(ships):
    hitteds = 0
    for ship in ships:
        if ship.hasBeenHitted():
            hitteds += 1

    return hitteds


def calculateSunk(ships):
    sunkeds = 0
    for ship in ships:
        if ship.hasBeenSunk():
            sunkeds += 1

    return sunkeds


def parseLetterToNumber(letter):
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
               "V", "W", "X", "Y", "Z"]

    return letters.index(letter) + 1

def numberToLetter(number):
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
               "V", "W", "X", "Y", "Z"]

    return letters[number - 1]


def solution(N, S, T):
    # INCOMPLETE, ONLY PROVIDES INFORMATION OF HITTED OR 1 SQUARE SHIPS SUNK

    # Create ships and shots arrays

    ships = S.split(",")

    shots = T.split(" ")

    shipsOnBoard = []

    for ship in ships:
        # Create objects for each ship and saving ships on this "board"

        sh = Ship(ship)
        shipsOnBoard.append(sh)

    for shot in shots:

        # Shooting ships

        for sh in shipsOnBoard:
            sh.hit(shot)

    # Calculated values is done with a function who calls methods of Ship
    # classes

    hitteds = calculateHitteds(shipsOnBoard)
    sunkeds = calculateSunk(shipsOnBoard)

    print("Hitten ships: ", hitteds)
    print("Sunken ships: ", sunkeds)

    return str(sunkeds) + "," + str(hitteds)


if __name__ == "__main__":
    solution(6, "A2 B1,A2 C2,D1 D4", "A1 B1 D2 A2 B2")